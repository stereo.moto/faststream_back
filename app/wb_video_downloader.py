import json
import logging
import os
import subprocess
import zipfile
from dataclasses import asdict
from io import BytesIO
import aiohttp
import requests
from faststream import FastStream, Logger
from faststream.nats import ConsumerConfig, JStream, NatsBroker, NatsMessage, PullSub
from PIL import Image, Image, ImageFont
from app.classes.classes import SkuFeedbacksVideoPayload, SkuFeedbacksVideoResult, SkuFeedbacksVideoZipPayload, \
    SkuFeedbacksVideoZipResult

MSG_PROCESSING_TIME = 30
broker = NatsBroker(
    ping_interval=5,
    graceful_timeout=MSG_PROCESSING_TIME * 1.2,
    log_level=logging.DEBUG,
)
app = FastStream(broker=broker)

logging.basicConfig(level=logging.INFO)

@broker.subscriber(
    subject="wb_download.feedback_video.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.feedback_video.in"], declare=False),
    durable="wb_download_video",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000))
async def download_feedback_video(msg: NatsMessage):
    input_data = SkuFeedbacksVideoPayload(**json.loads(msg.raw_message.data))
    query_id = input_data.query_id
    sku = input_data.sku
    video_id = input_data.video_id
    total = input_data.count_total
    await msg.in_progress()

    videos_folder = f"feedbacks/videos/{sku}/videos/"
    os.makedirs(videos_folder, exist_ok=True)
    servers = [f"https://videofeedback{i:02}.wb.ru" for i in range(1, 6)]

    videos = []
    norm_video_id = video_id.split('/')[1]  # Вырезаем часть "1/" или "2/"
    for server in servers:
        video_url = f"{server}/{norm_video_id}/index.m3u8"
        async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
            try:
                async with session.head(video_url) as response:
                    if response.status == 200:
                        video_filename = f"{sku}_feedback_video_{norm_video_id}.mp4"
                        video_path = os.path.join(videos_folder, video_filename)
                        try:
                            subprocess.run(["ffmpeg", "-i", video_url, "-c", "copy", "-y", video_path], check=True)
                            videos.append(video_filename)
                            await msg.ack()
                            break
                        except Exception as e:
                            logging.error(f"Error downloading video from {video_url}: {e}")
                            await msg.nack()

            except Exception as e:
                logging.error(f"Error downloading video from {video_url}: {e}")
                await msg.nack()


    js_payload = SkuFeedbacksVideoResult(
        sku=sku,
        videos=videos,
        query_id=query_id,
        count_total=total)
    bytes_ = json.dumps(asdict(js_payload)).encode()
    await broker.publish(subject=f"wb_download.feedback_video.{query_id}.out", message=bytes_)

@broker.subscriber(
    subject="wb_download.feedbacks_video_zip.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.feedbacks_video_zip.in"], declare=False),
    durable="wb_download_feedback_video_zipper",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000))
async def zip_videos(msg: NatsMessage):
    await msg.ack()
    input_data = SkuFeedbacksVideoZipPayload(**json.loads(msg.raw_message.data))
    sku = input_data.sku
    query_id = input_data.query_id
    zip_folder = f"feedbacks/videos/{sku}/zip/"
    os.makedirs(zip_folder, exist_ok=True)

    zip_archives = []
    zip_filename = f"{sku}_{query_id}_feedback_zip.zip"
    files_fs = os.listdir(f"feedbacks/videos/{sku}/videos/")
    fs_folder = (f"feedbacks/videos/{sku}/")
    logging.info(f"FILES_FS:{files_fs}")

    with zipfile.ZipFile(zip_folder+zip_filename, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for photo in files_fs:
            photo_path = os.path.join(fs_folder, photo)
            archive_name = os.path.relpath(photo_path, fs_folder)
            logging.info(f"Adding photo {photo_path} to zip archive as {archive_name}")
            try:
                zipf.write(photo_path, archive_name)
            except Exception as e:
                logging.error(f"Failed to add photo {photo_path} to zip archive: {e}")
                continue

    zip_archives.append(zip_filename)

    if zip_archives:
        js_payload = SkuFeedbacksVideoZipResult(
            sku=sku,
            files=zip_archives
        )
        bytes_ = json.dumps(asdict(js_payload)).encode()
        logging.debug(f"Sending zip archives: {zip_archives}")
        await broker.publish(subject=f"wb_download.feedbacks_video_zip.{query_id}.out", message=bytes_)

@app.after_startup
async def publish_sku_for_work() -> None:
    pass


def vol_video_host(e):
    if 0 <= e <= 47:
        t = "01"
    elif 48 <= e <= 95:
        t = "02"
    elif 96 <= e <= 143:
        t = "03"
    else:
        t = "04"
    return f"tvideobasket-{t}.wbbasket.ru"

def vol_feedback_host(e):
    if 0 <= e <= 431:
        t = "01"
    elif 432 <= e <= 863:
        t = "02"
    elif 864 <= e <= 1199:
        t = "03"
    elif 1200 <= e <= 1535:
        t = "04"
    elif 1536 <= e <= 1919:
        t = "05"
    elif 1920 <= e <= 2303:
        t = "06"
    else:
        t = "07"
    return f"feedback{t}.wbbasket.ru"

def construct_host_v2(e, t="nm"):
    r = int(e)
    n = r % 144 if t == "video" else int(r / 100000)
    a = int(r / 10000) if t == "video" else int(r / 1000)
    if t == "nm":
        o = vol_feedback_host(n)
    elif t == "feedback":
        o = vol_feedback_host(n)
    elif t == "video":
        o = vol_video_host(n)
    return f"https://{o}/vol{n}/part{a}/{r}"

def get_sku_basket(sku: str) -> str:
    sku = str(sku)
    vol = sku[:-5]
    part = sku[:-3]
    if int(vol) >= 0 and int(vol) <= 143:
        t = "01"
    elif int(vol) >= 144 and int(vol) <= 287:
        t = "02"
    elif int(vol) >= 288 and int(vol) <= 431:
        t = "03"
    elif int(vol) >= 432 and int(vol) <= 719:
        t = "04"
    elif int(vol) >= 720 and int(vol) <= 1007:
        t = "05"
    elif int(vol) >= 1008 and int(vol) <= 1061:
        t = "06"
    elif int(vol) >= 1062 and int(vol) <= 1115:
        t = "07"
    elif int(vol) >= 1116 and int(vol) <= 1169:
        t = "08"
    elif int(vol) >= 1170 and int(vol) <= 1313:
        t = "09"
    elif int(vol) >= 1314 and int(vol) <= 1601:
        t = "10"
    elif int(vol) >= 1602 and int(vol) <= 1655:
        t = "11"
    elif int(vol) >= 1656 and int(vol) <= 1919:
        t = "12"
    elif int(vol) >= 1920 and int(vol) <= 2045:
        t = "13"
    elif int(vol) >= 2046 and int(vol) <= 2189:
        t = "14"
    elif int(vol) >= 2190 and int(vol) <= 2405:
        t = "15"
    else:
        t = "16"
    return f"https://basket-{t}.wbbasket.ru/vol{int(vol)}/part{int(part)}/{sku}"

async def download_main_photo_async(sku: str) -> str:
    basket_url = get_sku_basket(sku)
    products_folder = "products/photos/"
    os.makedirs(products_folder, exist_ok=True)

    fs_filename = f"{sku}_main_photo.webp"
    fs_path = os.path.join(products_folder, fs_filename)

    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        try:
            async with session.get(f"{basket_url}/images/big/1.webp") as response:
                if response.status == 200:
                    with open(fs_path, 'wb') as f:
                        f.write(await response.read())
                    return fs_filename
                else:
                    print(f"Failed to download main photo for SKU {sku}: {response.status}")
                    return None
        except aiohttp.ClientError as e:
            print(f"An error occurred during main photo download for SKU {sku}: {e}")
            return None
