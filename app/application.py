import asyncio
import json
import logging
import os
import sys
import time
from dataclasses import dataclass, asdict
from datetime import datetime
from io import StringIO

import pandas as pd
from openai import OpenAI
from faststream import FastStream, Logger, ContextRepo
from faststream.nats import NatsBroker, PullSub, JStream, NatsMessage
from nats.js.api import ConsumerConfig
from pydantic import BaseModel, Field
from .utils.utils import download_main_photo_async, download_main_photo

openai_client = OpenAI(api_key="sk-proj-8v7msSRAB3ApOfxVj4rwT3BlbkFJQliYWPZppOh5hSjCVXEh")

@dataclass
class OpenAIPayload:
    task_uid: str
    model: str
    messages: list
    temperature: float
    max_tokens: int
    top_p: float
    frequency_penalty: float
    presence_penalty: float
    response_format: str

@dataclass
class OpenAIResult:
    task_uid: str
    response: str
    completion_tokens: int
    prompt_tokens: int
    worker: str

class Sku(BaseModel):
    sku: int = Field(..., description="sku of product")

class PhotoSku(BaseModel):
    sku: int = Field(..., description="sku of product")
    photo: str = Field(..., description="photo of product")

MSG_PROCESSING_TIME = 30

broker = NatsBroker(
    ping_interval=5,
    graceful_timeout=MSG_PROCESSING_TIME * 1.2,
    log_level=logging.INFO)

app = FastStream(broker)

@broker.subscriber(
    subject="openai.out",
    stream=JStream(name="openai_stream", subjects=["openai.out"], declare=False),
    durable="durable_ai_out",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # 30 seconds * 1.2
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000,  # explicit limit
    ),
)
async def process_msg_ai(msg: NatsMessage, logger: Logger) -> None:
    result = OpenAIResult(**json.loads(msg.raw_message.data))
    result_string = json.loads(result.response)
    with StringIO(result_string) as json_file:
        decoded_json = json.load(json_file)
        logger.info(f"Decoded JSON: {decoded_json}")
    df = pd.DataFrame()
    try:
        df = pd.json_normalize(decoded_json)
        df['completion_tokens'] = result.completion_tokens
        df['prompt_tokens'] = result.prompt_tokens
        df['task_uid'] = result.task_uid
        df['worker_id'] = result.worker
    except Exception as e:
        logger.info(f"Error: {e}")
    try:
        products_folder = f"products/openai/"
        os.makedirs(products_folder, exist_ok=True)
        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        fs_filename = f"{decoded_json['id']}_result_{timestamp}.csv"
        fs_path = os.path.join(products_folder, fs_filename)
        df.to_csv(fs_path, index=False)
    except:
        logging.info("File not found")
    await msg.ack()



@broker.subscriber(
    subject="wb_photos.in",
    stream=JStream(name="main_photo_stream", subjects=["wb_photos.in"], declare=False),
    durable="myowndurable",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # 30 seconds * 1.2
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000,  # explicit limit
    ),
)
async def process_msg(product: Sku, msg: NatsMessage, logger: Logger) -> None:
    logger.info(f"{os.getpid()} {msg.raw_message.metadata.num_delivered} {msg.raw_message.metadata.sequence.consumer} | {product.sku}| {msg.raw_message.metadata}")
    await msg.in_progress()
    start = time.time()
    picture = None
    picture = await download_main_photo_async(product.sku)
    while time.time() - start < MSG_PROCESSING_TIME:
        await asyncio.sleep(1)
        await msg.in_progress()
        if picture:
            await msg.raw_message.ack_sync()
            break
    if not picture:
        logger.info(f"ERROR {product.sku}: picture not downloaded")
        await msg.nack()


async def openai_process(client: OpenAI, messages: list, model: str, temperature: float, max_tokens: int, top_p: float, frequency_penalty: float, presence_penalty: float, response_format: str, task_uid: str, logger: Logger):
    try:
        response = client.chat.completions.create(
            model=model,
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens,
            top_p=top_p,
            frequency_penalty=frequency_penalty,
            presence_penalty=presence_penalty,
            response_format=response_format,
        )
        response_ai = response.choices[0].message.content
        # logging.info(f"OpenAI response: {json.dumps(response_ai)}")
        completion_tokens = response.usage.completion_tokens
        prompt_tokens = response.usage.prompt_tokens
        return {"response": json.dumps(response_ai), "task_uid": task_uid, "completion_tokens": completion_tokens,
                "prompt_tokens": prompt_tokens, "worker": "openai_worker_1"}
    except Exception as e:
        logger.info(f"Error: {e}")
        return None

@broker.subscriber(
    subject="openai.in",
    stream=JStream(name="openai_stream", subjects=["openai.in"], declare=False),
    durable="durable_ai",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # 30 seconds * 1.2
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000,  # explicit limit
    ),
)
async def process_msg_ai(msg: NatsMessage, logger: Logger) -> None:
    logger.info(f"{os.getpid()} {msg.raw_message.metadata.num_delivered} {msg.raw_message.metadata.sequence.consumer} | {msg.raw_message.metadata}")
    await msg.in_progress()
    start = time.time()
    openai_response = None
    payload = OpenAIPayload(**json.loads(msg.raw_message.data))
    #logger.info(f"Payload: {payload}")
    openai_response = await openai_process(
        client=openai_client,
        messages=payload.messages,
        model=payload.model,
        temperature=payload.temperature,
        max_tokens=payload.max_tokens,
        top_p=payload.top_p,
        frequency_penalty=payload.frequency_penalty,
        presence_penalty=payload.presence_penalty,
        response_format=payload.response_format,
        task_uid=payload.task_uid,
        logger=logger
    )
    while time.time() - start < MSG_PROCESSING_TIME:
        await asyncio.sleep(1)
        await msg.in_progress()
        if openai_response:
            await msg.raw_message.ack_sync()
            result_openai = OpenAIResult(
                task_uid=openai_response["task_uid"],
                response=openai_response["response"],
                completion_tokens=openai_response["completion_tokens"],
                prompt_tokens=openai_response["prompt_tokens"],
                worker="openai_worker_1")
            bytes_ = json.dumps(asdict(result_openai)).encode()
            #logger.info(f"json: {msg.raw_message.metadata}")
            await broker.publish(bytes_, subject="openai.out", stream="openai_stream")
            break
    if not openai_response:
        logger.info(f"ERROR {openai_response}: picture not downloaded")
        await msg.nack()

@broker.subscriber("wb_photos.out", stream="main_photo_stream")
async def wb_logger(logger: Logger, msg: PhotoSku):
        picture = (msg.photo)
        result = f"Result fot SKU {msg.sku}: picture {picture}"
        logger.info(result)
"""
@broker.subscriber("wb_photos", stream="main_photo_stream_download", max_workers=10, durable="worker2", pull_sub=PullSub(batch_size=10))
async def wb_downloader(logger: Logger, msg: Sku,  context: ContextRepo):
        if context.get(«wb_photos_{task_id}»):
            return await msg.reject()
        context.set_global(«wb_photos_{task_id}», 1)
        picture = await download_main_photo_async(msg.sku)
        result = f"SKU {msg.sku}: picture {picture} downloaded"
        logger.info(result)
        greeting = Greeting(greeting=result)
        await to_greetings.publish(result)

"""

# @broker.subscriber(subject="wb_photos.in", stream=JStream(name="main_photo_stream", subjects=["wb_photos.in"], declare=False),
#     durable="mydurable",
#     pull_sub=PullSub(batch_size=1, timeout=100.0),
#     config=ConsumerConfig(ack_wait=MSG_PROCESSING_TIME))
# async def wb_downloader(logger: Logger, product: Sku, message: NatsMessage):
#     await message.in_progress()
#     picture = await download_main_photo_async(product.sku, message)
#
#     start = time.time()
#     while time.time() - start < MSG_PROCESSING_TIME:
#         await asyncio.sleep(1)
#         await message.in_progress()
#         if picture:
#             await message.ack_sync()
#             break
#     await message.ack_sync()

    # if not picture:
    #     logger.info(f"ERROR {product.sku}: picture not downloaded")
    #     await message.reject()
    # else:
    #     await message.ack()
    # logger.info(f"SKU {product.sku}: picture downloaded")
#@broker.subscriber("wb_photos.in", stream="main_photo_stream", durable="download_photo_worker", pull_sub=PullSub(batch_size=3), config=ConsumerConfig(ack_wait=MSG_PROCESSING_TIME))
async def wb_downloader(logger: Logger, product: Sku, message: NatsMessage):
    await message.in_progress()
    picture = await download_main_photo_async(product.sku, message)
    if not picture:
        logger.info(f"ERROR {product.sku}: picture not downloaded")
        await message.reject()
    else:
        await message.ack()
    logger.info(f"SKU {product.sku}: picture downloaded")
    #await broker.publish(PhotoSku(sku=product.sku, photo=picture), subject="wb_photos.out", stream="main_photo_stream")
    # if context.get(f"wb_photos_{msg.sku}"):
    #     logger.info(f"Context found)")
    #     return await message.reject()
    # context.set_global(f"wb_photos_{msg.sku}", 1)

@app.after_startup
async def publish_sku_for_work() -> None:
    pass

