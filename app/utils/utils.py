import logging
import os
from datetime import datetime, time
import aiohttp
import asyncio

import requests
from faststream.nats import NatsMessage


def get_sku_basket(sku: int) -> str:
    vol = str(sku)[:-5]
    part = str(sku)[:-3]
    if int(vol) >= 0 and int(vol) <= 143:
        t = "01"
    elif int(vol) >= 144 and int(vol) <= 287:
        t = "02"
    elif int(vol) >= 288 and int(vol) <= 431:
        t = "03"
    elif int(vol) >= 432 and int(vol) <= 719:
        t = "04"
    elif int(vol) >= 720 and int(vol) <= 1007:
        t = "05"
    elif int(vol) >= 1008 and int(vol) <= 1061:
        t = "06"
    elif int(vol) >= 1062 and int(vol) <= 1115:
        t = "07"
    elif int(vol) >= 1116 and int(vol) <= 1169:
        t = "08"
    elif int(vol) >= 1170 and int(vol) <= 1313:
        t = "09"
    elif int(vol) >= 1314 and int(vol) <= 1601:
        t = "10"
    elif int(vol) >= 1602 and int(vol) <= 1655:
        t = "11"
    elif int(vol) >= 1656 and int(vol) <= 1919:
        t = "12"
    elif int(vol) >= 1920 and int(vol) <= 2045:
        t = "13"
    elif int(vol) >= 2046 and int(vol) <= 2189:
        t = "14"
    elif int(vol) >= 2190 and int(vol) <= 2405:
        t = "15"
    else:
        t = "16"
    return f"https://basket-{t}.wbbasket.ru/vol{int(vol)}/part{int(part)}/{sku}"

async def download_single_card_async(sku: int) -> dict:
    basket_url = get_sku_basket(sku)
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        async with session.get(f"{basket_url}/info/ru/card.json") as response:
            logging.info(f"Downloading card for SKU {basket_url}")
            card_data = await response.json()
    return card_data


async def download_single_card_extended_async(sku: int, root_sku_id: int) -> dict:
    sku_basket_variant = get_sku_basket(sku)
    output = []
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        async with session.get(f"{sku_basket_variant}/info/ru/card.json") as response:
            logging.info(f"Downloading card for SKU {sku_basket_variant}")
            card_data = await response.json()
            sku_nmId_variant = card_data.get('imt_id', None)
            sku_name_variant = card_data.get('imt_name', None)
            output.append({
                "sku": sku,
                "basket_url": sku_basket_variant,
                "card": card_data,
                "name": sku_name_variant,
                "nmid": sku_nmId_variant,
                "root_sku_id": int(root_sku_id)
            })
    return output

def download_main_photo(sku: int) -> list:
    sku_basket = get_sku_basket(sku)
    products_folder = f"products/photos/"
    os.makedirs(products_folder, exist_ok=True)
    # Скачиваем и сохраняем фотографии
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    fs_filename = f"{sku}_main_photo_{timestamp}.webp"
    fs_path = os.path.join(products_folder, fs_filename)
    with open(fs_path, 'wb') as f:
        main_photo = requests.get(f"{sku_basket}/images/big/1.webp")
        f.write(main_photo.content)
    return fs_filename

async def download_main_photo_async(sku: int) -> str:
    basket_url = get_sku_basket(sku)
    products_folder = "products/photos/"
    os.makedirs(products_folder, exist_ok=True)
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    fs_filename = f"{sku}_main_photo_{timestamp}.webp"
    fs_path = os.path.join(products_folder, fs_filename)
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        try:
            async with session.get(f"{basket_url}/images/big/1.webp") as response:
                if response.status == 200:
                    with open(fs_path, 'wb') as f:
                        f.write(await response.read())
                    return fs_filename
                else:
                    logging.error(f"Failed to download main photo for SKU {sku}: {response.status}")
                    return None
        except aiohttp.ClientError as e:
            logging.error(f"An error occurred during main photo download for SKU {sku}: {e}")
            return None


