import json
import logging
from dataclasses import asdict
import aiohttp
from faststream import FastStream, Logger
from faststream.nats import ConsumerConfig, JStream, NatsBroker, NatsMessage, PullSub
from app.classes.classes import SkuListPayload, SkuMainPhotoResult, SkuCardResult, SkuFeedbackPayload, SkuFeedbacksResult, \
    SkuFeedbacksImagePayload, SkuFeedbacksImageResult, SkuFeedbacksImageZipResult, SkuFeedbacksImageZipPayload


MSG_PROCESSING_TIME = 30
broker = NatsBroker(
    ping_interval=5,
    graceful_timeout=MSG_PROCESSING_TIME * 1.2,
    log_level=logging.DEBUG,
)
app = FastStream(broker=broker)

logging.basicConfig(level=logging.INFO)

@broker.subscriber(
    subject="wb_download.card_json.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.card_json.in"], declare=False),
    durable="wb_download_card_json",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000),)
async def card_data_downloader(msg: NatsMessage):
    logging.info(f"msg from bot, {msg}")
    input_data = SkuListPayload(**json.loads(msg.raw_message.data))
    query_id = input_data.query_id
    await msg.ack()
    for sku in input_data.sku:
        card_data = await download_single_card_async(sku)
        if card_data:
            payload_js = SkuCardResult(
                sku=sku,
                card_json=card_data,
                query_id=query_id
            )
            bytes_ = json.dumps(asdict(payload_js)).encode()
            await broker.publish(subject=f"wb_download.card_json.{query_id}.out", message=bytes_)
            logging.info(card_data)
        else:
            logging.error("Failed to download card")
            return

@broker.subscriber(
    subject="wb_download.feedbacks_json.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.feedbacks_json.in"], declare=False),
    durable="wb_download_feedbacks_json",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000), )
async def main_feedback_downloader(msg: NatsMessage):
    input_data = SkuFeedbackPayload(**json.loads(msg.raw_message.data))
    query_id = input_data.query_id
    sku = input_data.sku
    nmid = input_data.nmid
    await msg.ack()
    api_url = await download_feedbacks_api_url(nmid)
    logging.info(f"Downloading feedbacks for SKU {api_url}")
    json_feedbacks = await download_feedbacks_json(api_url)
    if json_feedbacks:
        payload_js = SkuFeedbacksResult(
            sku=sku,
            query_id=query_id,
            feedback_json=json_feedbacks
        )
        bytes_ = json.dumps(asdict(payload_js)).encode()
        await broker.publish(subject=f"wb_download.feedbacks_json.{query_id}.out", message=bytes_)
        #logging.info(f"Count feedbacks : {count_feedbacks}")
    else:
        logging.error("Failed to download feedbacks")

@broker.subscriber(
    subject="wb_download.card_json_variants.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.card_json_variants.in"], declare=False),
    durable="wb_download_json_variants",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(

        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000))
async def card_data_variants_downloader (msg: NatsMessage):
    input_data = SkuListPayload(**json.loads(msg.raw_message.data))
    query_id = input_data.query_id
    for sku in input_data.sku:
        card_data = await download_single_card_async(sku)
        if card_data:
            await msg.ack()
            #await msg.ack()
            payload_js = SkuCardResult(
                sku=sku,
                card_json=card_data,
                query_id=query_id
            )
            bytes_ = json.dumps(asdict(payload_js)).encode()
            await broker.publish(subject=f"wb_download.card_json_variants.{query_id}.out", message=bytes_)
            logging.info(card_data)
        else:
            await msg.nak()
            logging.error("Failed to download card")
            return

@app.after_startup
async def publish_sku_for_work() -> None:
    pass

def get_sku_basket(sku: str) -> str:
    sku = str(sku)
    vol = sku[:-5]
    part = sku[:-3]
    if int(vol) >= 0 and int(vol) <= 143:
        t = "01"
    elif int(vol) >= 144 and int(vol) <= 287:
        t = "02"
    elif int(vol) >= 288 and int(vol) <= 431:
        t = "03"
    elif int(vol) >= 432 and int(vol) <= 719:
        t = "04"
    elif int(vol) >= 720 and int(vol) <= 1007:
        t = "05"
    elif int(vol) >= 1008 and int(vol) <= 1061:
        t = "06"
    elif int(vol) >= 1062 and int(vol) <= 1115:
        t = "07"
    elif int(vol) >= 1116 and int(vol) <= 1169:
        t = "08"
    elif int(vol) >= 1170 and int(vol) <= 1313:
        t = "09"
    elif int(vol) >= 1314 and int(vol) <= 1601:
        t = "10"
    elif int(vol) >= 1602 and int(vol) <= 1655:
        t = "11"
    elif int(vol) >= 1656 and int(vol) <= 1919:
        t = "12"
    elif int(vol) >= 1920 and int(vol) <= 2045:
        t = "13"
    elif int(vol) >= 2046 and int(vol) <= 2189:
        t = "14"
    elif int(vol) >= 2190 and int(vol) <= 2405:
        t = "15"
    else:
        t = "16"
    return f"https://basket-{t}.wbbasket.ru/vol{int(vol)}/part{int(part)}/{sku}"


async def download_single_card_async(sku: str) -> dict:
    basket_url = get_sku_basket(sku)
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        async with session.get(f"{basket_url}/info/ru/card.json") as response:
            logging.info(f"Downloading card for SKU {sku}")
            card_data = await response.json()
    return card_data

async def download_feedbacks_api_url(imt_id: str) -> dict:
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        async with session.get(f"https://feedback-bt.wildberries.ru/feedback/api/v1/host?imt={imt_id}") as response:
            api_data = await response.json()
    return api_data[0]

async def download_feedbacks_json(url: str) -> dict:
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        async with session.get(url) as response:
            feedbacks_json = await response.json()
    return feedbacks_json