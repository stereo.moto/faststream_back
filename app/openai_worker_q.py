import asyncio
import os
from pprint import pprint
import time
from app.classes.classes import OpenAIResult, OpenAIPayload
from openai import OpenAI
from dotenv import load_dotenv, find_dotenv

import json
import logging
from dataclasses import asdict
import aiohttp
from faststream import FastStream, Logger
from faststream.nats import ConsumerConfig, JStream, NatsBroker, NatsMessage, PullSub


MSG_PROCESSING_TIME = 60
broker = NatsBroker(
    ping_interval=5,
    graceful_timeout=MSG_PROCESSING_TIME * 1.2,
    log_level=logging.DEBUG,
)
app = FastStream(broker=broker)

logging.basicConfig(level=logging.INFO)
load_dotenv(find_dotenv())
openai_client = OpenAI(api_key=os.getenv("OPENAI_API_KEY"))

async def openai_process(client: OpenAI, messages: list, model: str, temperature: float, max_tokens: int, top_p: float, frequency_penalty: float, presence_penalty: float, response_format: str):
    try:
        response = client.chat.completions.create(
            model=model,
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens,
            top_p=top_p,
            frequency_penalty=frequency_penalty,
            presence_penalty=presence_penalty,
            response_format=response_format,
        )
        response_ai = response.choices[0].message.content
        #logging.info(f"OpenAI response: {json.dumps(response_ai)}")
        completion_tokens = response.usage.completion_tokens
        prompt_tokens = response.usage.prompt_tokens
        return {"response": json.dumps(response_ai), "completion_tokens": completion_tokens,
                "prompt_tokens": prompt_tokens, "worker": "openai_worker_1", "code": 200}
    except Exception as e:
        logging.error(f"Error: {e}")
        return None
@broker.subscriber(
    subject="wb_ai_text.single_text.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_ai_text.single_text.in"], declare=False),
    durable="wb_ai_text_single_text",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    #queue="wb_ai_text_queue",
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1))
async def process_msg_ai(msg: NatsMessage):
    logging.info(f"{os.getpid()}")
    await msg.in_progress()
    start = time.time()
    openai_response = None
    payload = OpenAIPayload(**json.loads(msg.raw_message.data))
    logging.info(f"Payload: sent")

    openai_response = await openai_process(
        client=openai_client,
        messages=payload.messages,
        model=payload.model,
        temperature=payload.temperature,
        max_tokens=payload.max_tokens,
        top_p=payload.top_p,
        frequency_penalty=payload.frequency_penalty,
        presence_penalty=payload.presence_penalty,
        response_format=payload.response_format
    )
    logging.info(f"OpenAI response: {openai_response}")
    while time.time() - start < MSG_PROCESSING_TIME:
        pprint(f"time countdown: {time.time() - start}")
        await msg.in_progress()
        if openai_response:
            logging.info(f"OpenAI got response: {payload.task_id}")
            total_cost = openai_response["completion_tokens"]/1000000*payload.completion_cost + openai_response["prompt_tokens"]/1000000*payload.prompt_cost
            await msg.ack()
            logging.info(f"Total cost: {payload.task_id}")
            result_openai = OpenAIResult(
                task_id=payload.task_id,
                response=openai_response["response"],
                completion_tokens=openai_response["completion_tokens"],
                prompt_tokens=openai_response["prompt_tokens"],
                worker=os.getpid(),
                sku=payload.sku,
                sku_name=payload.sku_name,
                total_price=total_cost,
                code=200,
                model=payload.model,
                original_text=payload.original_text,
                score=payload.score,
                original_size=payload.original_size
            )
            bytes_ = json.dumps(asdict(result_openai)).encode()
            logging.info(f"sent data wb_ai_text.single_text.{payload.task_id}.out")
            await broker.publish(subject=f"wb_ai_text.single_text.{payload.task_id}.out", message=bytes_)
            break
        await asyncio.sleep(1)
    if not openai_response:
        logging.info(f"ERROR {openai_response}: openai not downloaded")
        await msg.nack(delay=10)


@broker.subscriber(
    subject="wb_ai_text.single_text.*.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_ai_text.single_text.*.in"], declare=False),
    durable="wb_ai_text_single_text_2",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    #queue="wb_ai_text_queue_2",
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1))
async def process_msg_ai(msg: NatsMessage):
    logging.info(f"{os.getpid()}")
    await msg.in_progress()
    start = time.time()
    openai_response = None
    payload = OpenAIPayload(**json.loads(msg.raw_message.data))
    logging.info(f"Payload: sent")

    openai_response = await openai_process(
        client=openai_client,
        messages=payload.messages,
        model=payload.model,
        temperature=payload.temperature,
        max_tokens=payload.max_tokens,
        top_p=payload.top_p,
        frequency_penalty=payload.frequency_penalty,
        presence_penalty=payload.presence_penalty,
        response_format=payload.response_format
    )
    logging.info(f"OpenAI response: {openai_response}")
    while time.time() - start < MSG_PROCESSING_TIME:
        pprint(f"time countdown: {time.time() - start}")
        await msg.in_progress()
        if openai_response:
            logging.info(f"OpenAI got response: {payload.task_id}")
            total_cost = openai_response["completion_tokens"]/1000000*payload.completion_cost + openai_response["prompt_tokens"]/1000000*payload.prompt_cost
            await msg.ack()
            logging.info(f"Total cost: {payload.task_id}")
            result_openai = OpenAIResult(
                task_id=payload.task_id,
                response=openai_response["response"],
                completion_tokens=openai_response["completion_tokens"],
                prompt_tokens=openai_response["prompt_tokens"],
                worker=os.getpid(),
                sku=payload.sku,
                sku_name=payload.sku_name,
                total_price=total_cost,
                code=200,
                model=payload.model,
                original_text=payload.original_text,
                score=payload.score,
                original_size=payload.original_size
            )
            bytes_ = json.dumps(asdict(result_openai)).encode()
            logging.info(f"sent data wb_ai_text.single_text.{payload.task_id}.out")
            await broker.publish(subject=f"wb_ai_text.single_text.{payload.task_id}.out", message=bytes_)
            break
        await asyncio.sleep(1)
    if not openai_response:
        logging.info(f"ERROR {openai_response}: openai not downloaded")
        await msg.nack(delay=10)



@app.after_startup
async def publish_sku_for_work() -> None:
    pass
