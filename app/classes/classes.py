from dataclasses import dataclass

@dataclass
class SkuFeedbackPayload:
    sku: str
    query_id: int
    nmid: int

@dataclass
class SkuListPayload:
    sku: list
    query_id: int
@dataclass
class SkuMainPhotoResult:
    sku: str
    filename: str
    query_id: int
    count_total: int

@dataclass
class SkuCardResult:
    sku: str
    card_json: dict
    query_id: int

@dataclass
class SkuFeedbacksResult:
    sku: str
    feedback_json: dict
    query_id: int

@dataclass
class SkuFeedbacksImagePayload:
    sku: str
    image_id: dict
    query_id: str
    count_total: int

@dataclass
class SkuFeedbacksImageResult:
    sku: str
    images: dict
    query_id: str
    count_total: int
    original_image_id: int

@dataclass
class SkuFeedbacksImageZipPayload:
    sku: str
    query_id: str

@dataclass
class SkuFeedbacksImageZipResult:
    sku: str
    files: dict


@dataclass
class SkuFeedbacksVideoPayload:
    sku: str
    video_id: dict
    query_id: str
    count_total: int

@dataclass
class SkuFeedbacksVideoResult:
    sku: str
    videos: dict
    query_id: str
    count_total: int

@dataclass
class SkuFeedbacksVideoZipPayload:
    sku: str
    query_id: str

@dataclass
class SkuFeedbacksVideoZipResult:
    sku: str
    files: dict


@dataclass
class OpenAIPayload:
    task_id: str
    model: str
    messages: list
    temperature: float
    max_tokens: int
    top_p: float
    frequency_penalty: float
    presence_penalty: float
    response_format: str
    total: int
    sku: str
    sku_name: str
    completion_cost: float
    prompt_cost: float
    original_text: str
    score: int
    original_size: str

@dataclass
class OpenAIResult:
    task_id: str
    response: str
    completion_tokens: int
    prompt_tokens: int
    worker: str
    code: int
    model: str
    sku: str
    sku_name: str
    total_price: int
    original_text: str
    score: int
    original_size: str

@dataclass
class OpenAIEmbeddingPayload:
    task_id: str
    model: str
    text: str
    prompt_cost: float
@dataclass
class OpenAIEmbeddingResult:
    task_id: int
    embedding: list
    total_cost: float
    text: str

