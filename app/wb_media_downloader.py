import json
import logging
import os
import zipfile
from dataclasses import asdict
from io import BytesIO
import aiohttp
from faststream import FastStream, Logger
from faststream.nats import ConsumerConfig, JStream, NatsBroker, NatsMessage, PullSub
from PIL import Image, Image, ImageFont
from app.classes.classes import SkuListPayload, SkuMainPhotoResult, SkuCardResult, SkuFeedbackPayload, SkuFeedbacksResult, \
    SkuFeedbacksImagePayload, SkuFeedbacksImageResult, SkuFeedbacksImageZipResult, SkuFeedbacksImageZipPayload


MSG_PROCESSING_TIME = 30
broker = NatsBroker(
    ping_interval=5,
    graceful_timeout=MSG_PROCESSING_TIME * 1.2,
    log_level=logging.DEBUG,
)
app = FastStream(broker=broker)

logging.basicConfig(level=logging.INFO)

@broker.subscriber(
    subject="wb_download.product_picture.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.product_picture.in"], declare=False),
    durable="wb_download_picture",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000),)
async def main_photo_downloader(msg: NatsMessage):
    input_data = SkuListPayload(**json.loads(msg.raw_message.data))
    query_id = input_data.query_id
    await msg.ack()
    for sku in input_data.sku:
        main_photo = await download_main_photo_async(sku)
        if main_photo:
            payload_js = SkuMainPhotoResult(
                sku=sku,
                filename=main_photo,
                query_id=query_id,
                count_total=len(input_data.sku)
            )
            bytes_ = json.dumps(asdict(payload_js))
            await broker.publish(subject=f"wb_download.product_picture.{query_id}.out", message=bytes_)
            logging.info(main_photo)
        else:
            print("Failed to download main photo")

@broker.subscriber(
    subject="wb_download.feedback_image.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.feedback_image.in"], declare=False),
    durable="wb_download_feedback_image",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000))
async def download_feedback_photo(msg: NatsMessage):
    input_data = SkuFeedbacksImagePayload(**json.loads(msg.raw_message.data))
    query_id = input_data.query_id
    sku = input_data.sku
    photo_id = input_data.image_id
    total = input_data.count_total
    await msg.ack()

    fs_folder = f"feedbacks/photos/{sku}/fs/"
    #ms_folder = f"feedbacks/photos/{sku}/ms/"
    os.makedirs(fs_folder, exist_ok=True)
    #os.makedirs(ms_folder, exist_ok=True)

    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False), ) as session:
        fs_url = construct_host_v2(photo_id, "feedback") + "/photos/fs.webp"
        #ms_url = construct_host_v2(photo_id, "feedback") + "/photos/ms.webp"
        fs_filename = f"{sku}_feedback_photo_{photo_id}_fs.webp"
        #ms_filename = f"{sku}_feedback_photo_{photo_id}_ms.webp"
        fs_path = os.path.join(fs_folder, fs_filename)
        #ms_path = os.path.join(ms_folder, ms_filename)

        images = []
        try:
            async with session.get(fs_url) as response:
                if response.status == 200:
                    with open(fs_path, 'wb') as f:
                        logging.info(f"Downloaded photo {fs_url}")
                        f.write(await response.read())
                        images.append(fs_path)
                else:
                    logging.error(f"Failed to download photo from {fs_url}. Status code: {response.status}")
        except Exception as e:
            logging.error(f"Error downloading photo from {fs_url}: {e}")

        # try:
        #     async with session.get(ms_url) as response:
        #         if response.status == 200:
        #             with open(ms_path, 'wb') as f:
        #                 logging.info(f"Downloaded photo {ms_url}")
        #                 f.write(await response.read())
        #                 images.append(ms_path)
        #         else:
        #             logging.error(f"Failed to download photo from {ms_url}. Status code: {response.status}")
        # except Exception as e:
        #     logging.error(f"Error downloading photo from {fs_url}: {e}")

    js_payload = SkuFeedbacksImageResult(
        sku=sku,
        images=images,
        query_id=query_id,
        count_total=total,
        original_image_id=photo_id
    )
    bytes_ = json.dumps(asdict(js_payload)).encode()
    await broker.publish(subject=f"wb_download.feedback_image.{query_id}.out", message=bytes_)

@broker.subscriber(
    subject="wb_download.feedback_image.*.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.feedback_image.*.in"], declare=False),
    durable="wb_download_feedback_image_2",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000))
async def download_feedback_photo(msg: NatsMessage):
    input_data = SkuFeedbacksImagePayload(**json.loads(msg.raw_message.data))
    query_id = input_data.query_id
    sku = input_data.sku
    photo_id = input_data.image_id
    total = input_data.count_total
    await msg.ack()

    fs_folder = f"feedbacks/photos/{sku}/fs/"
    #ms_folder = f"feedbacks/photos/{sku}/ms/"
    os.makedirs(fs_folder, exist_ok=True)
    #os.makedirs(ms_folder, exist_ok=True)

    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False), ) as session:
        fs_url = construct_host_v2(photo_id, "feedback") + "/photos/fs.webp"
        #ms_url = construct_host_v2(photo_id, "feedback") + "/photos/ms.webp"
        fs_filename = f"{sku}_feedback_photo_{photo_id}_fs.webp"
        #ms_filename = f"{sku}_feedback_photo_{photo_id}_ms.webp"
        fs_path = os.path.join(fs_folder, fs_filename)
        #ms_path = os.path.join(ms_folder, ms_filename)

        images = []
        try:
            async with session.get(fs_url) as response:
                if response.status == 200:
                    with open(fs_path, 'wb') as f:
                        logging.info(f"Downloaded photo {fs_url}")
                        f.write(await response.read())
                        images.append(fs_path)
                else:
                    logging.error(f"Failed to download photo from {fs_url}. Status code: {response.status}")
        except Exception as e:
            logging.error(f"Error downloading photo from {fs_url}: {e}")

        # try:
        #     async with session.get(ms_url) as response:
        #         if response.status == 200:
        #             with open(ms_path, 'wb') as f:
        #                 logging.info(f"Downloaded photo {ms_url}")
        #                 f.write(await response.read())
        #                 images.append(ms_path)
        #         else:
        #             logging.error(f"Failed to download photo from {ms_url}. Status code: {response.status}")
        # except Exception as e:
        #     logging.error(f"Error downloading photo from {fs_url}: {e}")

    js_payload = SkuFeedbacksImageResult(
        sku=sku,
        images=images,
        query_id=query_id,
        count_total=total,
        original_image_id=photo_id
    )
    bytes_ = json.dumps(asdict(js_payload)).encode()
    await broker.publish(subject=f"wb_download.feedback_image.{query_id}.out", message=bytes_)


@broker.subscriber(
    subject="wb_download.feedbacks_photo_zip.in",
    stream=JStream(name="wb_reviews_bot", subjects=["wb_download.feedbacks_photo_zip.in"], declare=False),
    durable="wb_download_feedback_image_zipper",
    pull_sub=PullSub(batch_size=1, timeout=100.0),
    config=ConsumerConfig(
        ack_wait=MSG_PROCESSING_TIME * 1.2,  # wait a little bit more than processing
        max_ack_pending=-1,  # explicit limit
        max_waiting=1000))
async def zip_photos(msg: NatsMessage):
    await msg.ack()
    input_data = SkuFeedbacksImageZipPayload(**json.loads(msg.raw_message.data))
    sku = input_data.sku
    query_id = input_data.query_id
    zip_folder = f"feedbacks/photos/{sku}/zip/"
    os.makedirs(zip_folder, exist_ok=True)

    zip_archives = []
    zip_filename = f"{sku}_feedback_photo_{query_id}.zip"
    files_fs = os.listdir(f"feedbacks/photos/{sku}/fs/")
    fs_folder = (f"feedbacks/photos/{sku}/fs/")
    logging.info(f"FILES_FS:{files_fs}")

    with zipfile.ZipFile(zip_folder+zip_filename, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for photo in files_fs:
            photo_path = os.path.join(fs_folder, photo)
            archive_name = os.path.relpath(photo_path, fs_folder)
            logging.info(f"Adding photo {photo_path} to zip archive as {archive_name}")
            try:
                zipf.write(photo_path, archive_name)
            except Exception as e:
                logging.error(f"Failed to add photo {photo_path} to zip archive: {e}")
                continue

    zip_archives.append(zip_filename)

    # zip_filename = f"{sku}_feedback_photo_ms.zip"
    # files_ms = os.listdir(f"feedbacks/photos/{sku}/ms/")
    # ms_folder = (f"feedbacks/photos/{sku}/ms/")
    # logging.info(f"FILES_MS:{files_ms}")
    # with zipfile.ZipFile(os.path.join(zip_folder, zip_filename), 'w') as zipf:
    #     for photo in files_ms:
    #         photo = os.path.join(ms_folder, photo)
    #         try:
    #             zipf.write(photo, os.path.relpath(photo, ms_folder))
    #         except Exception as e:
    #             logging.error(f"Failed to add photo {photo} to zip archive: {e}")
    #             continue
    # zip_archives.append(zip_filename)

    if zip_archives:
        js_payload = SkuFeedbacksImageZipResult(
            sku=sku,
            files=zip_archives
        )
        bytes_ = json.dumps(asdict(js_payload)).encode()
        logging.debug(f"Sending zip archives: {zip_archives}")
        await broker.publish(subject=f"wb_download.feedbacks_photo_zip.{query_id}.out", message=bytes_)


@app.after_startup
async def publish_sku_for_work() -> None:
    pass


def vol_video_host(e):
    if 0 <= e <= 47:
        t = "01"
    elif 48 <= e <= 95:
        t = "02"
    elif 96 <= e <= 143:
        t = "03"
    else:
        t = "04"
    return f"tvideobasket-{t}.wbbasket.ru"

def vol_feedback_host(e):
    if 0 <= e <= 431:
        t = "01"
    elif 432 <= e <= 863:
        t = "02"
    elif 864 <= e <= 1199:
        t = "03"
    elif 1200 <= e <= 1535:
        t = "04"
    elif 1536 <= e <= 1919:
        t = "05"
    elif 1920 <= e <= 2303:
        t = "06"
    else:
        t = "07"
    return f"feedback{t}.wbbasket.ru"

def construct_host_v2(e, t="nm"):
    r = int(e)
    n = r % 144 if t == "video" else int(r / 100000)
    a = int(r / 10000) if t == "video" else int(r / 1000)
    if t == "nm":
        o = vol_feedback_host(n)
    elif t == "feedback":
        o = vol_feedback_host(n)
    elif t == "video":
        o = vol_video_host(n)
    return f"https://{o}/vol{n}/part{a}/{r}"

def get_sku_basket(sku: str) -> str:
    sku = str(sku)
    vol = sku[:-5]
    part = sku[:-3]
    if int(vol) >= 0 and int(vol) <= 143:
        t = "01"
    elif int(vol) >= 144 and int(vol) <= 287:
        t = "02"
    elif int(vol) >= 288 and int(vol) <= 431:
        t = "03"
    elif int(vol) >= 432 and int(vol) <= 719:
        t = "04"
    elif int(vol) >= 720 and int(vol) <= 1007:
        t = "05"
    elif int(vol) >= 1008 and int(vol) <= 1061:
        t = "06"
    elif int(vol) >= 1062 and int(vol) <= 1115:
        t = "07"
    elif int(vol) >= 1116 and int(vol) <= 1169:
        t = "08"
    elif int(vol) >= 1170 and int(vol) <= 1313:
        t = "09"
    elif int(vol) >= 1314 and int(vol) <= 1601:
        t = "10"
    elif int(vol) >= 1602 and int(vol) <= 1655:
        t = "11"
    elif int(vol) >= 1656 and int(vol) <= 1919:
        t = "12"
    elif int(vol) >= 1920 and int(vol) <= 2045:
        t = "13"
    elif int(vol) >= 2046 and int(vol) <= 2189:
        t = "14"
    elif int(vol) >= 2190 and int(vol) <= 2405:
        t = "15"
    else:
        t = "16"
    return f"https://basket-{t}.wbbasket.ru/vol{int(vol)}/part{int(part)}/{sku}"

async def download_main_photo_async(sku: str) -> str:
    basket_url = get_sku_basket(sku)
    products_folder = "products/photos/"
    os.makedirs(products_folder, exist_ok=True)

    fs_filename = f"{sku}_main_photo.webp"
    fs_path = os.path.join(products_folder, fs_filename)

    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        try:
            async with session.get(f"{basket_url}/images/big/1.webp") as response:
                if response.status == 200:
                    with open(fs_path, 'wb') as f:
                        f.write(await response.read())
                    return fs_filename
                else:
                    print(f"Failed to download main photo for SKU {sku}: {response.status}")
                    return None
        except aiohttp.ClientError as e:
            print(f"An error occurred during main photo download for SKU {sku}: {e}")
            return None

def process_photo(image_bytes: bytes):
    image = Image.open(BytesIO(image_bytes))
    d1 = Image.Draw(image)
    font = ImageFont.truetype("roboto.ttf", 40)
    d1.text(xy=(10, 10), text="XYI", font=font, fill=(255, 0, 0))
    buff = BytesIO()
    image.save(buff, format="JPEG")
    buff.seek(0)
    return buff.read()